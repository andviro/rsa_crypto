#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
from ez_setup import use_setuptools
use_setuptools()
from setuptools import setup

setup_params = dict(name='RSA-Crypto',
                    version='0.0.3',
                    packages=[str('rsa_crypto')],
                    author='Andrew Rodionoff',
                    author_email='andviro@gmail.com',
                    install_requires=['m2crypto', 'pies'],
                    setup_requires=['nose', 'coverage'],
                    license='LGPL',
                    description='Simple wrapper around M2Crypto for RSA cryptography',
                    )

setup(**setup_params)
