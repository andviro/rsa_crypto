#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals, print_function
from pies.overrides import *  # noqa
from rsa_crypto import (encrypt, decrypt, sign, verify, SignatureError,
                        is_rsa_certificate, _autoload)
import os
from io import BytesIO
from nose.tools import raises

cert = b'''-----BEGIN CERTIFICATE-----
MIIDuzCCAqOgAwIBAgIJAJ/dkXicEpWKMA0GCSqGSIb3DQEBCwUAMHQxCzAJBgNV
BAYTAlJVMQ8wDQYDVQQIDAZSdXNzaWExDzANBgNVBAcMBkthbHVnYTEQMA4GA1UE
CgwHQW5kdmlybzEZMBcGA1UEAwwQQW5kcmV3IFJvZGlvbm9mZjEWMBQGCSqGSIb3
DQEJARYHYW5kdmlybzAeFw0xNDExMDUwODE1MDlaFw0xNDEyMDUwODE1MDlaMHQx
CzAJBgNVBAYTAlJVMQ8wDQYDVQQIDAZSdXNzaWExDzANBgNVBAcMBkthbHVnYTEQ
MA4GA1UECgwHQW5kdmlybzEZMBcGA1UEAwwQQW5kcmV3IFJvZGlvbm9mZjEWMBQG
CSqGSIb3DQEJARYHYW5kdmlybzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAOS+2f7Kn3m0ZCxtalGkPlgYRRE8XFONBcWQ036wDjRAk9HYPmbJ98Z4LvTl
LIIdogNqe0bciC3ZOJWh6/ERmcgPGri8mUJ23UuYTn4V5NxboztnRbVf5o5M/ai0
M6uz0J8Gyp0Dda00VjGiL1ywIF/Z8B/27TRK+wCNBjelMPmvSZFiD8FXpJzXGMiV
ALT+Iv56oYvraAdKzQoBTJZz4h54+iYUWBUmSfe2ZAzenYdR3LR/StlQoRA+FKlY
9VJEhsxEZU2X4ZBF5QtJBs+6hQGIghwBaCF4mnMR2DFtcb7iAdZWH7y37FLLfYb+
ahCZwhVjfG1z9O46yH7L1REtS/0CAwEAAaNQME4wHQYDVR0OBBYEFNraXd/FTYdC
ZZo0U21CC8NCDl5YMB8GA1UdIwQYMBaAFNraXd/FTYdCZZo0U21CC8NCDl5YMAwG
A1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAKjRCjRwyPO9s6MCXVvTXQi6
ESR8u1KhDj48vyA77xW6xPY/z/JmdonWdsmDn7Tqwn2RS30a6qciI4jkRxwkh+zA
CMOGlPC6z1cgOLM/yiMex+NZ9kYC7BwCMyXnum/xm5kFB6yR1B0xVROERkGLsoBR
WI6BPWZDnGejbh6ZY54j2QGxQh8LeCIFOf8kCLLAQfm6A4P4y86LMFLlN9nTCuaB
wMmcZzTfEztjOSpixvkpcB5olfVAVX3o2fG8LupFC0Uu8pKNAfXqWx7lCkGZB44d
PhyK+70dld9uHC90VjKF6xuVRyP+sTucsqLXC6n9WRBnaQRTurceyFZ4PsrKvUw=
-----END CERTIFICATE-----
'''

key = b'''-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDkvtn+yp95tGQs
bWpRpD5YGEURPFxTjQXFkNN+sA40QJPR2D5myffGeC705SyCHaIDantG3Igt2TiV
oevxEZnIDxq4vJlCdt1LmE5+FeTcW6M7Z0W1X+aOTP2otDOrs9CfBsqdA3WtNFYx
oi9csCBf2fAf9u00SvsAjQY3pTD5r0mRYg/BV6Sc1xjIlQC0/iL+eqGL62gHSs0K
AUyWc+IeePomFFgVJkn3tmQM3p2HUdy0f0rZUKEQPhSpWPVSRIbMRGVNl+GQReUL
SQbPuoUBiIIcAWgheJpzEdgxbXG+4gHWVh+8t+xSy32G/moQmcIVY3xtc/TuOsh+
y9URLUv9AgMBAAECggEAZbDO2u2bThC7A+E0hcz9hk5rzgjGhsJH0Zfb3hNIjUQ0
MvLucuXQyCO/xFFpowKndttMqKj7xV4vQJ42dPo9ImpMl9oITDrpkGTKaFU1ZB09
CPiVTdeveI2Tqv9JhUiwMzJiusDB/NkqhHA0nAEUs2gAz0UIGNpIkesf23cNucGy
/UQZoWvCktWjrhU5kvR8RfPDP9uqT5IxfjO1pmHR/q8y0bb6tIi97aWezglG+wi1
Ttzgy5O/KpNwmFYwecdQ6SlPiSp7dEOGvC76y6oLcwNAnZQI4aewA9V2ThV9XhV+
BSLyfktCiUtpe/+jW9QOFngkgzMztop5kyDbRqy5hQKBgQD/MZM8VS88Rc2lIN1b
Skcs1Puc8UV9iU/yOqVKs7yG6tzH8EDHAI56FCwfeFak3RUIO/yQTrI/7E/hurN9
LSMHNnkloin4vYZuggCTD9MTDonhnbBY2ZDsoSlx9VgMk+cOA5JXnYXYT6dBEtkn
WIE4CNyb+Y3DN4Dymi2HADBEbwKBgQDld+H0lC/A19ALOlUxw9QmH++ZmCvHlJ5m
k4dzNOcZiwbzLORjuBU4ZysoKIZGfqQNi0TCW41sZKyYmJmroqID34wi+hiqFqtM
L99LFoZMbfeFkpBxWg2j7iudwwFAjHqB/r4OWgiNxfOYUtoLIWaOlAh2gjlX5Og1
6tpm/VCkUwKBgCmyG/B7qqAad6NHX2OPG1oD4qid81CzotBdLKyr+V3B+hjg9HSP
brgbm6n4q498X/dcdcPOV+mB3vtu2ezV9ycyi1DO0jTGIrTjoNxmdYJjmbu3XBzo
T3maS39HKv/DGrl7c+nE3x2cxiMnxTtchqB1MjZ56+O9qd/5uP6Z1UkZAoGAUdSF
nJ23xdJwW4Cq3vtRzWK4JBLzEdI7BqN3uyrTevVsPg31SGZeEuYfqsuAzuTchgPz
OkhBz7S7JBPPQTjXBACVTefYIdfBsIKl43Yd4GWazmRs5C9UC/CkVzcyXB18lp0M
k7uhr0U3UEvDfBKmKxQ5TGG2J1T3RjsBNZcCSgcCgYEA5xM5M5RFJX2dMJdBoDKH
42Mp0L7XWgwbUkMu2RpmNlFbgCkGHWijTFJcomzHyuTnywPF4XW8UjXFxllkxK1C
8g+GFI09MKAcTCOLVbadyoXrmhwiAS3Z+hMn3zjq89Ix57U+A7dSXY/U2mAWzhFJ
OJDca8zHH2XqSlQNXMUps4Y=
-----END PRIVATE KEY-----
'''


def test_autoload():
    c1 = _autoload(cert)
    b1 = BytesIO(cert)
    c2 = _autoload(b1)
    assert c1 == c2


@raises(IOError)
def test_autoload_file():
    _autoload('non-existant-file')


@raises(TypeError)
def test_autoload_bad():
    _autoload({})


def test_sign_verify():
    data = os.urandom(1024)
    signed = sign(data, cert, key)
    extracted, thumb = verify(signed)
    assert extracted == data


@raises(SignatureError)
def test_sign_verify_bad():
    data = os.urandom(1024)
    verify(data)


@raises(SignatureError)
def test_detached_sig_bad():
    data = os.urandom(1024)
    wrong_data = os.urandom(1024)
    signature = sign(data, cert, key, include_data=False)
    verify(wrong_data, signature)


def test_detached_sig():
    data = os.urandom(1024)
    signature = sign(data, cert, key, include_data=False)
    extracted, thumb = verify(data, signature)
    assert extracted == data


def test_encrypt_decrypt():
    data = os.urandom(1024)
    encrypted = encrypt(data, cert)
    decrypted = decrypt(encrypted, cert, key)
    assert decrypted == data


gost_cert = b'''-----BEGIN CERTIFICATE-----
MIIC8jCCAqGgAwIBAgIKT7DRQAACAATigjAIBgYqhQMCAgMwZTEgMB4GCSqGSIb3
DQEJARYRaW5mb0BjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRMwEQYDVQQKEwpD
UllQVE8tUFJPMR8wHQYDVQQDExZUZXN0IENlbnRlciBDUllQVE8tUFJPMB4XDTEz
MDgxNTE4MDYwMFoXDTE0MTAwNDA3MDk0MVowHzEdMBsGA1UEAwwUQ1NQIFRlc3Qg
Y2VydGlmaWNhdGUwYzAcBgYqhQMCAhMwEgYHKoUDAgIkAAYHKoUDAgIeAQNDAARA
vmFyRFyuPcs+CQiPtWsHoMqzH0xAJSmvwdapsfUt3usZMTz4L06o7kr8ydT/Wzek
sNltPPbZcIHketyR9FKpxqOCAXUwggFxMAsGA1UdDwQEAwIEkDAdBgNVHSUEFjAU
BggrBgEFBQcDBAYIKwYBBQUHAwIwCQYDVR0gBAIwADAdBgNVHQ4EFgQUZmIbTdGL
k8H8o/aHbh3cKvbCMcEwHwYDVR0jBBgwFoAUbY9eBdlfrJEXlB6VmgUwODd6ECow
VQYDVR0fBE4wTDBKoEigRoZEaHR0cDovL3d3dy5jcnlwdG9wcm8ucnUvQ2VydEVu
cm9sbC9UZXN0JTIwQ2VudGVyJTIwQ1JZUFRPLVBSTygyKS5jcmwwgaAGCCsGAQUF
BwEBBIGTMIGQMDMGCCsGAQUFBzABhidodHRwOi8vd3d3LmNyeXB0b3Byby5ydS9v
Y3NwbmMvb2NzcC5zcmYwWQYIKwYBBQUHMAKGTWh0dHA6Ly93d3cuY3J5cHRvcHJv
LnJ1L0NlcnRFbnJvbGwvcGtpLXNpdGVfVGVzdCUyMENlbnRlciUyMENSWVBUTy1Q
Uk8oMikuY3J0MAgGBiqFAwICAwNBADd3hPIwE9jc7n65V5Yp9Ku6qLMUy0Oawd/g
mfrMdfbo0GmrEmnQoQsef4plOA8Ybc5QMGKrNAUQhqlld3RJSFg=
-----END CERTIFICATE-----
'''


def test_is_rsa_certificate():
    assert is_rsa_certificate(cert)
    assert not is_rsa_certificate(gost_cert)
