#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from pies.overrides import *  # noqa
from M2Crypto import BIO, SMIME, X509, m2

from . import _autoload


class SignatureError(Exception):
    pass


def verify(data, signature=None):
    '''
    Извлечение и проверка подписи CMS-сообщения. Бросает исключение, если
    подпись не бьется с сертификатом из сообщения.

    * `data` -  Проверяемое сообщение в виде файло-подобного объекта,
        пути к файлу или байтовой строки
    * `signature` - Отсоединенная подпись для проверки. Если параметр не задан,
        и данные и подпись берутся из первого параметра.

    Возвращает пару вида: (данные, отпечаток сертификата). Бросает исключение
    `SignatureError`, если подпись не сходится.

    '''
    flags = SMIME.PKCS7_NOVERIFY
    data_bio = BIO.MemoryBuffer(_autoload(data))
    try:
        if signature:
            p7_bio = BIO.MemoryBuffer(_autoload(signature))
            p7 = SMIME.PKCS7(m2.pkcs7_read_bio_der(p7_bio._ptr()), 1)
            flags += SMIME.PKCS7_DETACHED
        else:
            p7 = SMIME.PKCS7(m2.pkcs7_read_bio_der(data_bio._ptr()), 1)
            data_bio = None
        certStack = p7.get0_signers(X509.X509_Stack())
        thumb = certStack[0].get_fingerprint('sha1')
        st = X509.X509_Store()
        mime = SMIME.SMIME()
        mime.set_x509_store(st)
        mime.set_x509_stack(certStack)
        result = mime.verify(p7, data_bio=data_bio, flags=flags)
    except Exception as exc:
        raise SignatureError(exc.message)
    return result, thumb


def sign(data, cert, key, include_data=True):
    '''
    Подписывание данных сертификатом и упаковка в сообщение PKCS7

    * `data` - подписываемые данные в виде файло-подобного объекта, пути к файлу или байтовой строки
    * `cert` - сертификат для подписи, формат -- см. выше
    * `key` - закрытый ключ для подписи, формат -- см. выше
    * `include_data` - признак включения исходных данных в подпись, включен по умолчанию

    Возвращает CMS-сообщение, закодированное в DER, в виде байтовой строки

    '''
    flags = SMIME.PKCS7_BINARY
    if not include_data:
        flags += SMIME.PKCS7_DETACHED
    buf = BIO.MemoryBuffer(_autoload(data))
    mime = SMIME.SMIME()
    key_bio = BIO.MemoryBuffer(_autoload(key))
    cert_bio = BIO.MemoryBuffer(_autoload(cert))
    mime.load_key_bio(key_bio, cert_bio)
    pkcs7 = mime.sign(buf, flags=flags)
    out = BIO.MemoryBuffer()
    pkcs7.write_der(out)
    return out.read()


def encrypt(data, cert, alg='des_ede3_cbc'):
    '''
    Шифрование данных на сертификат получателя

    * `data` -  данные в виде байтовой строки, файла или пути к файлу
    * `cert` - сертификат получателя, формат см. выше
    * `alg` - Алгоритм шифрования, по умолчанию 3-key triple DES CBC
      Список алгоритмов можно посмотреть в мануале на OpenSSL.

    Возвращает зашифрованные данные в виде байтовой строки, формат DER

    '''
    buf = BIO.MemoryBuffer(_autoload(data))
    mime = SMIME.SMIME()
    cert_bio = BIO.MemoryBuffer(_autoload(cert))
    cert = X509.load_cert_bio(cert_bio)
    sk = X509.X509_Stack()
    sk.push(cert)
    mime.set_x509_stack(sk)
    mime.set_cipher(SMIME.Cipher(alg))

    pkcs7 = mime.encrypt(buf, flags=SMIME.PKCS7_BINARY)
    out = BIO.MemoryBuffer()
    pkcs7.write_der(out)
    return out.read()


def decrypt(data, cert, key):
    '''
    Дешифрование

    * `data` -  данные в виде байтовой строки, файла или пути к файлу
    * `cert` - сертификат получателя, формат см. выше
    * `alg` - Алгоритм шифрования, по умолчанию 3-key triple DES CBC

    Возвращает зашифрованные данные в виде байтовой строки, формат DER

    '''
    inp = BIO.MemoryBuffer(_autoload(data))
    mime = SMIME.SMIME()
    key_bio = BIO.MemoryBuffer(_autoload(key))
    cert_bio = BIO.MemoryBuffer(_autoload(cert))
    mime.load_key_bio(key_bio, cert_bio)
    p7 = SMIME.PKCS7(m2.pkcs7_read_bio_der(inp._ptr()), 1)
    return mime.decrypt(p7, flags=SMIME.PKCS7_BINARY)
