#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function

from . import _autoload
from M2Crypto import BIO, X509


def is_rsa_certificate(cert):
    '''
    Проверка, является сертификат RSA или ГОСТ
    * `cert` -  Сертификат в виде файло-подобного объекта,
        пути к файлу или байтовой строки, формат PEM или DER

    Возвращает True, если сертификат имеет открытый ключ в формате RSA.

    '''

    cert_bio = BIO.MemoryBuffer(_autoload(cert))
    cert = X509.load_cert_bio(cert_bio)
    pk = cert.get_pubkey()
    try:
        pk.get_rsa()
    except ValueError:
        return False
    return True
