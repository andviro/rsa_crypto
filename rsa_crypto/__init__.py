#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function

def _autoload(data):
    '''
    Автоматическая загрузка бинарника из файла, файлового объекта или строки.

    '''
    if isinstance(data, str):
        return open(data, 'rb').read()
    elif isinstance(data, bytes):
        return data
    elif hasattr(data, 'read'):
        return data.read()
    else:
        raise TypeError("Input data of type '{0}' not supported"
                        .format(data.__class__.__name__))

from .pkcs7 import *  # noqa
from .cert import *  # noqa
